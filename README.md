## What is PogoProg ? 

PogoProg is a range of spring-loaded programming adapters from Electronut Labs. These are designed with aesthetics and ergonomics in mind, and can be in a variety of production and prototyping situations. Pogo pins are expensive and inconvenient to source in small quantities, so we believe that providing a pre-soldered inexpensive programmer could be beneficial.

### PogoProg Model A

Pogoprog Model A is designed for AVR-ISP programmers. You can program any board with 2x3 programming headers. 

![PogoProg ModelA](PogoProg_modelA.jpg)

**Dimensions**

| Dimension |      value    |
|----------|:-------------:|
| Size      |  30 x 20 | 
| Pin Pitch |    2.54 mm   | 

<hr>

### PogoProg Model B

 PogoProg Model B is designed for use with such a 4-pin 2.54mm header that uses the *SWDIO*, *SWDCLK*, *VDD* and *GND* pins. (Of course you can use this programmer for any 4 signal lines, as long as the pitch is compatible.)

![PogoProg Model B](PogoProg_modelB.jpg)

Pogo Pin Programmer with 2.54 mm pitch - perfect for SWD programming.



**Dimensions**

| Dimension |      value    |
|----------|:-------------:|
| Size      |  29.55 x 42.55 | 
| Pin Pitch |    2.54 mm   | 

<hr> 

### PogoProg Model C

PogoProg Model C is designed with 1.27mm
Pitch for SWD programmers. You can also use it for 4 signal lines if the pitch is compatible.


![PogoProg Model C](PogoProg_modelC.jpg)

**Dimensions**

| Dimension |      value    |
|----------|:-------------:|
| Size      |  32.90 x 23.80 | 
| Pin Pitch |    1.27 mm   | 

<hr>

### PogoProg Model D

Pogoprog Model A is designed for AVR-ISP programmers with 1.27mm pitch. You can program any board with 2x3 programming headers if the pitch is compatible. 

![PogoProg Model D](PogoProg_modelD.jpg)

**Dimensions**

| Dimension |      value    |
|----------|:-------------:|
| Size      |  30 x 20 | 
| Pin Pitch |    1.27 mm   | 

<hr>

### Using PogoProg

PogoProg can be used as shown in the image below. We are using PogoProg Model B to program hackaBLE.   

![PogoProg Model B](PogoProg-modelB-2.jpg)

## Datasheet

You can download Pogoprog datasheet from here. [PogoProg Datasheet](https://gitlab.com/electronutlabs-public/ElectronutLabs-PogoProg/raw/master/PogoProg%20Datasheet.pdf?inline=false)

## Buy a PogoProg!

PogoProg is available for purchase from our [Tindie store][1]. Please email us at **info@electronut.in** if you have any questions.

[1]: https://www.tindie.com/stores/ElectronutLabs/

## Code Repository

You can find all  design files related to PogoProg at the link below:

[https://gitlab.com/electronutlabs-public/ElectronutLabs-PogoProg](https://gitlab.com/electronutlabs-public/ElectronutLabs-PogoProg)

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More information at our [website](https://electronut.in).